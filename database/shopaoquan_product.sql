-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: shopaoquan
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `idcate` int(11) DEFAULT NULL,
  `name` tinytext,
  `price` tinytext,
  `img` tinytext,
  `info` tinytext,
  `amount` int(11) DEFAULT NULL,
  `madein` tinytext,
  `producer` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,1,'AT-185','315.000đ','img/imgsp/AT185.jpg','-Đầy đủ kích thước(L -XL-XXL-XXXL), vải tốt, thích hợp cho người trẻ tuổi.',12,'Việt Nam','Việt Tiến'),(2,1,'AT-186','300.000đ','img/imgsp/AT186.jpg','-Đầy đủ kích thước(L -XL-XXL-XXXL), vải tốt, thích hợp cho người trẻ tuổi.',10,'Việt Nam','Việt Tiến'),(3,1,'AT-187','275.000đ','img/imgsp/AT187.jpg','-Đầy đủ kích thước(L -XL-XXL-XXXL), vải tốt, thích hợp cho người trẻ tuổi.',11,'Trung Quốc','Quảng Châu'),(4,1,'AT-188','265.000đ','img/imgsp/AT188.jpg','-Đầy đủ kích thước(L -XL-XXL-XXXL), vải tốt, thích hợp cho người trẻ tuổi.',12,'Việt Nam','Nhà Bè'),(5,1,'AT-189','310.000đ','img/imgsp/AT189.jpg','-Đầy đủ kích thước(L -XL-XXL-XXXL), vải tốt, thích hợp cho người trẻ tuổi.',14,'Mỹ','Mỹ'),(6,2,'ATT-3283','415.000đ','img/imgsp/ATT3283.jpg','-Đầy đủ kích thước(L -XL-XXL-XXXL), vải tốt, thích hợp cho người trẻ tuổi.',14,'Việt Nam','Nhà Bè'),(7,2,'ATT-3284','395.000đ','img/imgsp/ATT3284.jpg','-Áo thun tay dài, phù hợp cho mọi lứa tuổi',15,'Mỹ','Mỹ'),(8,2,'ATT-3286','410.000đ','img/imgsp/ATT3286.jpg','-Áo thun tay dài, phù hợp cho mọi lứa tuổi',15,'Mỹ','Mỹ'),(9,2,'ATT-3287','380.000đ','img/imgsp/ATT3287.jpg','-Áo thun tay dài, phù hợp cho mọi lứa tuổi',12,'Việt Nam','Nhà Bè'),(10,2,'ATT-3288','405.000đ','img/imgsp/ATT3288.jpg','-Áo thun tay dài, phù hợp cho mọi lứa tuổi',13,'Trung Quốc','Quảng Châu'),(11,3,'SMT-879','255.000đ','img/imgsp/SMT879.jpg','-Áo sơ mi ngăn cao cấp, phù hợp cho mọi lứa tuổi.',13,'Việt Nam','Nhà Bè'),(12,3,'SMT-880','260.000đ','img/imgsp/SMT880.jpg','-Áo sơ mi ngăn cao cấp, phù hợp cho mọi lứa tuổi.',15,'Trung Quốc','Quảng Châu'),(13,3,'SMT-881','240.000đ','img/imgsp/SMT881.jpg','-Áo sơ mi ngăn cao cấp, phù hợp cho mọi lứa tuổi.',15,'Trung Quốc','Quảng Châu'),(14,3,'SMT-882','255.000đ','img/imgsp/SMT882.jpg','-Áo sơ mi ngăn cao cấp, phù hợp cho mọi lứa tuổi.',15,'Mỹ','Mỹ'),(15,3,'SMT-883','235.000đ','img/imgsp/SMT883.jpg','-Áo sơ mi ngăn cao cấp, phù hợp cho mọi lứa tuổi.',16,'Mỹ','Mỹ'),(16,4,'SM225','320.000đ','img/imgsp/SM225.jpg','-Áo sơ mi dài, phù hợp cho công việc văn phòng.',16,'Trung Quốc','Quảng Châu'),(17,4,'SM226','310.000đ','img/imgsp/SM226.jpg','-Áo sơ mi dài, phù hợp cho công việc văn phòng.',5,'Việt Nam','Việt Tiến'),(18,4,'SM227','310.000đ','img/imgsp/SM227.jpg','-Áo sơ mi dài, phù hợp cho công việc văn phòng.',4,'Thái Lan','Thái Lan'),(19,4,'SM228','335.000đ','img/imgsp/SM228.jpg','-Áo sơ mi dài, phù hợp cho công việc văn phòng.',35,'Thái Lan','Thái Lan'),(20,4,'SM229','335.000đ','img/imgsp/SM229.jpg','-Áo sơ mi dài, phù hợp cho công việc văn phòng.',46,'Việt Nam','Việt Tiến'),(21,5,'QSK-406','195.000đ','img/imgsp/QSK406.jpg','-Quần tây nam, đầy đủ kích thước 30 đến 34, phù hợp với thanh thiếu niên trẻ',13,'Trung Quốc','Quảng Châu'),(22,5,'QSK-408','180.000đ','img/imgsp/QSK408.jpg','-Quần tây nam, đầy đủ kích thước 30 đến 34, phù hợp với thanh thiếu niên trẻ',34,'Trung Quốc','Quảng Châu'),(23,5,'QSK-410','315.000đ','img/imgsp/QSK410.jpg','-Quần tây nam, đầy đủ kích thước 30 đến 34, phù hợp với thanh thiếu niên trẻ',43,'Mỹ','Mỹ'),(24,5,'QSK-411','310.000đ','img/imgsp/QSK411.jpg','-Quần tây nam, đầy đủ kích thước 30 đến 34, phù hợp với thanh thiếu niên trẻ',31,'Việt Nam','Nhà Bè'),(25,5,'QSK-412','300.000đ','img/imgsp/QSK412.jpg','-Quần tây nam, đầy đủ kích thước 30 đến 34, phù hợp với thanh thiếu niên trẻ',13,'Mỹ','Mỹ'),(26,7,'QSJ-11','300.000đ','img/imgsp/QSJ11.jpg','-Quần jean ngắn cao cấp',14,'Việt Nam','Việt Tiến'),(27,8,'JEAN-124','175.000đ','img/imgsp/jean124.jpg','-Quần jean dài cao cấp',46,'Việt Nam','Nhà Bè'),(28,8,'JEAN-125','350.000đ','img/imgsp/jean125.jpg','-Quần jean dài cao cấp',31,'Mỹ','Mỹ'),(29,6,'QT-61','290.000đ','img/imgsp/QT61.jpg','-Quần tây nam, đầy đủ kích thước 30 đến 34, phù hợp với thanh thiếu niên trẻ',24,'Việt Nam','Việt Tiến');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-04 22:25:56
