package DAO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;

public class ConnectionDB {
	Connection conn;
	Statement stsm;
	PreparedStatement pst;
	public ConnectionDB() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/shopaoquan","root","0168805221");
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}catch(SQLException se) {
			se.printStackTrace();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public void showDB(ResultSet rs) {
		try{
			while(rs.next()) {
				System.out.printf("%10s",rs.getString(4));
			}
		}catch(SQLException e) {
			System.out.println("loi");
		}
	}
	public ResultSet getData(String sql) {
		stsm = null;
		try {
			stsm = conn.createStatement();
			return stsm.executeQuery(sql);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public ResultSet getDataByIDCate(String str) {
		 pst = null;
		ResultSet rs = null;
		String sqlCommand = "select * from product"+" where `idcate` = ?";
		try {
			pst = conn.prepareStatement(sqlCommand);
			pst.setString(1, str);
			rs = pst.executeQuery();
			return rs;
		}catch(SQLException e) {
			System.out.println("error"+e.toString());
		}
		return null;
	}
	
	public boolean updateData(String sql) {
		stsm= null;
		try {
			stsm = conn.createStatement();
			return stsm.executeUpdate(sql) > 0 ? true:false;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
/*public class ConnectionDB{
	private final String className = "com.mysql.jdbc.Driver";
	private final String url = "jdbc:mysql://localhost:3306/cuahang";
	private final String user = "root";
	private final String pass = "0168805221";
	
	private Connection connection;
	public void connect() {
		try {
			Class.forName(className);
			connection = DriverManager.getConnection(url, user, pass);
			System.out.println("connect sucess!");
		}catch(ClassNotFoundException e) {
			System.out.println("class not found");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			
			System.out.println("Error connection!");
		} 
	}*/
	/*public static void main(String[] args) {
		ConnectionDB connectionDB = new ConnectionDB();
		//connectionDB.showDB(connectionDB.getData("select * from product"));
		connectionDB.showDB(connectionDB.getData("select * from product"));
		connectionDB.showDB(connectionDB.getDataByIDCate("2"));
	}*/
}
