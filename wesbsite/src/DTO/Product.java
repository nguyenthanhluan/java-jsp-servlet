package DTO;

import java.io.Serializable;

public class Product implements Serializable{
	private int id;
	private int idcate;
	private String name;
	private String price;
	private String img;
	private String info;
	private int amount;
	private String madein;
	private String producer;
	
	/*contructor*/
	public Product() {
		
	}
	
	public Product(int id, int idcate,String name, String price, String img, String info, int amount, String madein, String producer)
	 {
		 super();
		 this.id = id;
		 this.idcate = idcate;
		 this.name = name;
		 this.price = price;
		 this.img = img;
		 this.info = info;
		 this.amount = amount;
		 this.madein = madein;
		 this.producer = producer;
	 }
	/*getter*/
	public int getId() 
	{
		return id;
	}
	public int getIdcate() 
	{
		return idcate;
	}
	public String getName() 
	{
		return name;
	}
	public String getPrice() 
	{
		return price;
	}
	public String getImg() 
	{
		return img;
	}
	public String getInfo() 
	{
		return info;
	}
	public int getAmount() 
	{
		return amount;
	}
	public String getMadein() 
	{
		return madein;
	}
	public String getProducer() 
	{
		return producer;
	}
	/*setter*/
	public void setId(int id) 
	{
		this.id = id;
	}
	public void setIdcate(int idcate) 
	{
		this.idcate = idcate;
	}
	public void setName(String name) 
	{
		this.name = name;
	}
	public void setPrice(String price) 
	{
		this.price = price;
	}
	public void setImg(String img) 
	{
		this.img = img;
	}
	public void setInfo(String info) 
	{
		this.info = info;
	}
	public void amount(int amount) 
	{
		this.amount = amount;
	}
	public void setMadein(String madein) 
	{
		this.madein = madein;
	}
	public void setProducer(String producer) 
	{
		this.producer = producer;
	}
}
