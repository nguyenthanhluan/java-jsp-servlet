package DTO;

import java.io.Serializable;

public class Category implements Serializable{
	private int idcate;
	private String name;
	public Category() {
		
	}
	
	public Category(int idcate, String name)
	 {
		 super();
		 this.idcate = idcate;
		 this.name = name;
	 }
	/*getter*/
	public int getIdcate() 
	{
		return idcate;
	}
	public String getName() 
	{
		return name;
	}
	/*setter*/
	public void setIdcate(int idcate) 
	{
		this.idcate = idcate;
	}
	public void setName(String name) 
	{
		this.name = name;
	}
}
